inserir registro na base de ponto para meu teste
insert into ponto (data_hora_batida, tipo_batida, usuario_id_usuario) values ("2020-07-07 20:41:34", "Entrada" , 1);
insert into ponto (data_hora_batida, tipo_batida, usuario_id_usuario) values ("2020-07-07 21:41:34", "Saída" , 1);
insert into ponto (data_hora_batida, tipo_batida, usuario_id_usuario) values ("2020-07-07 22:00:00", "Entrada" , 1);
insert into ponto (data_hora_batida, tipo_batida, usuario_id_usuario) values ("2020-07-07 22:30:00", "Saída" , 1);

###########
Incluir Usuário
    localhost:8080/usuario
    
    JSON:
    {
	"nomeCompleto": "Nome do Usuario",
	"cpf": "33731164817",
	"email": "usuario@usuario",
	"dataCadastro": "2020-07-05"
    }
###########
Consultar Dados do Usuario
    localhost:8080/usuario/2  (onde o 2 é o IdUsuario)
###########
Atualizar Usuário:
    localhost:8080/usuario/4  (onde o 1 é o idUsuario)
    
    JSON:
    {
	"nomeCompleto": "Nome do Usuario",
	"cpf": "33731164817",
	"email": "usuario@usuario",
	"dataCadastro": "2020-07-05"
    }
    
###########
Bater ponto
    localhost:8080/ponto/1/baterponto (onde o IdUsuario)
    
###########
Consultar Ponto Usuario Específico:
    localhost:8080/ponto/1   ( o 1 é apenas um exemplo)