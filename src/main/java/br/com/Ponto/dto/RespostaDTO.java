package br.com.Ponto.dto;

import br.com.Ponto.models.Ponto;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;


public class RespostaDTO {

    private List<Ponto> ponto;
    private double horasTrabalhadas;

    public RespostaDTO (List<Ponto> ponto){
        long minutosTrabalhadas = 0;
        LocalTime inicio = LocalTime.MIN;
        LocalTime fim = LocalTime.MAX;
        String entrada = "Entrada";

        Ponto pontoObjeto = new Ponto();
        this.ponto = ponto;


        for (int i = 0; i < ponto.size(); i++){
             pontoObjeto = ponto.get(i);
            if (pontoObjeto.getTipoBatida().equals(entrada)){
                inicio = pontoObjeto.getDataHoraBatida().toLocalTime();
            } else {
                fim = pontoObjeto.getDataHoraBatida().toLocalTime();
                Duration duracao = Duration.between(inicio, fim);
                minutosTrabalhadas += duracao.toMinutes();
            }
        }
        horasTrabalhadas = (double)minutosTrabalhadas / 60;
    }

    public RespostaDTO() {
    }

    public Iterable<Ponto> getPonto() {
        return ponto;
    }

    public void setPonto(List<Ponto> ponto) {
        this.ponto = ponto;
    }

    public double getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(int horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }
}
