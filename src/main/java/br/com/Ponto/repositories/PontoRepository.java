package br.com.Ponto.repositories;

import br.com.Ponto.models.Ponto;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Integer> {
      Iterable<Ponto> findAllByUsuarioIdUsuario(Integer idUsuario);
}
