package br.com.Ponto.services;

import br.com.Ponto.models.Ponto;
import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class PontoService {

    @Autowired
    PontoRepository pontoRepository;

    @Autowired
    UsuarioService usuarioService;

    public Ponto efetuarBatidaPonto(int idUsuario, Ponto ponto){
        Usuario usuario = usuarioService.buscarPorIdUsuario(idUsuario);
        ponto.setUsuario(usuario);
        LocalDateTime datahora = LocalDateTime.now();
        ponto.setDataHoraBatida(datahora);
        Ponto pontoObjeto = pontoRepository.save(ponto);
        return pontoObjeto;
    }

    public List<Ponto> buscarPorId(Integer idUsuario){
        Iterable<Ponto> pontos = pontoRepository.findAllByUsuarioIdUsuario(idUsuario);

        return (List) pontos;
    }

}
