package br.com.Ponto.services;

import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario salvarUsuario(Usuario usuario){
        LocalDate dataCadastral = LocalDate.now();
        usuario.setDataCadastro(dataCadastral);
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }


    public Iterable<Usuario> buscarTodos(){
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return  usuarios;
    }

    public Usuario buscarPorIdUsuario(int idUsuario){
        Optional<Usuario> optionalUsuario = usuarioRepository.findById(idUsuario);
        if (optionalUsuario.isPresent()){
            return optionalUsuario.get();
        }
        throw new RuntimeException("O usuário não foi encontrado!");
    }


    public Usuario atualizarUsuario(int idUsuario, Usuario usuario){
        if (usuarioRepository.existsById(idUsuario)){
            usuario.setIdUsuario(idUsuario);
            Usuario usuarioObjeto = usuarioRepository.save(usuario);
            return usuarioObjeto;
        }
        throw new RuntimeException("O usuário não foi encontrado para ser atualizado");
    }
}
