package br.com.Ponto.controllers;

import br.com.Ponto.dto.RespostaDTO;
import br.com.Ponto.models.Ponto;
import br.com.Ponto.repositories.PontoRepository;
import br.com.Ponto.services.PontoService;
import br.com.Ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/ponto")
public class PontoController {

    @Autowired
    private PontoService pontoService;

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/{idUsuario}/baterponto")
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto registrarPonto(@PathVariable(name = "idUsuario")int idUsuario, @RequestBody Ponto ponto){
        try {
            Ponto pontoObjeto = pontoService.efetuarBatidaPonto(idUsuario, ponto);
            return pontoObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{idUsuario}")
    public RespostaDTO exibirPonto (@PathVariable(name = "idUsuario", required = false) Integer idUsuario){
        try {
            List<Ponto> pontos = pontoService.buscarPorId(idUsuario);
            RespostaDTO respostaDTO = new RespostaDTO(pontos);
            return respostaDTO;

        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
