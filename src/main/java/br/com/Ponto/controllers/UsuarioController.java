package br.com.Ponto.controllers;

import br.com.Ponto.models.Usuario;
import br.com.Ponto.services.PontoService;
import br.com.Ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody Usuario usuario){
        return usuarioService.salvarUsuario(usuario);
    }

    @GetMapping
    public  Iterable<Usuario> exibirTodos (){
            Iterable<Usuario> usuarios = usuarioService.buscarTodos();
            return usuarios;
    }

    @GetMapping("/{idUsuario}")
    public Usuario exibirUsuario (@PathVariable(name = "idUsuario", required = false) Integer idUsuario){

        try {
            Usuario usuarioObjeto = usuarioService.buscarPorIdUsuario(idUsuario);
            return usuarioObjeto;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{idUsuario}")
    public Usuario atualizarUsuario (@PathVariable(name = "idUsuario") int idUsuario, @RequestBody Usuario usuario){
        try {
            Usuario usuarioObjeto = usuarioService.atualizarUsuario(idUsuario, usuario);
            return usuarioObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
