package br.com.Ponto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idUsuario;

    private String nomeCompleto;

    private String cpf;

    @Email(message = "O formado do email está errado")
    private String email;

    @JsonIgnoreProperties
    private LocalDate dataCadastro;

    public Usuario() {
    }

    public Usuario(int idUsuario, String nomeCompleto, @org.hibernate.validator.constraints.br.CPF(message = "O cpf está em formato errado") String cpf, @Email(message = "O formado do email está errado") String email, @NotNull(message = "A data de nascimento precisa estar no formato AAAA-MM-DD") LocalDate dataCadastro) {
        this.idUsuario = idUsuario;
        this.nomeCompleto = nomeCompleto;
        this.cpf = cpf;
        this.email = email;
        this.dataCadastro = dataCadastro;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

}
