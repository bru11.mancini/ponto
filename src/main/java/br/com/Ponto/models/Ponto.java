package br.com.Ponto.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Ponto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPonto;

    @JsonIgnoreProperties
    private LocalDateTime dataHoraBatida;

    private String tipoBatida;


    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    public Ponto() {
    }

    public Ponto(int idPonto, LocalDateTime dataHoraBatida, String tipoBatida) {
        this.idPonto = idPonto;
        this.dataHoraBatida = dataHoraBatida;
        this.tipoBatida = tipoBatida;
    }

    public int getIdPonto() {
        return idPonto;
    }

    public void setIdPonto(int idPonto) {
        this.idPonto = idPonto;
    }

    public LocalDateTime getDataHoraBatida() {
        return dataHoraBatida;
    }

    public void setDataHoraBatida(LocalDateTime dataHoraBatida) {
        this.dataHoraBatida = dataHoraBatida;
    }

    public String getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(String tipoBatida) {
        this.tipoBatida = tipoBatida;
    }



    public Usuario getUsuario(){
        return usuario;
    }

    public void setUsuario(Usuario usuario){
        this.usuario = usuario;
    }

}
