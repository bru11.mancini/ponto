package br.com.Ponto.Controllers;

import br.com.Ponto.controllers.PontoController;
import br.com.Ponto.dto.RespostaDTO;
import br.com.Ponto.models.Ponto;
import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.PontoRepository;
import br.com.Ponto.repositories.UsuarioRepository;
import br.com.Ponto.services.PontoService;
import br.com.Ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@WebMvcTest(controllers = PontoController.class)
public class PontoControllerTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private PontoService pontoService;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    Ponto ponto;
    Ponto ponto2;
    Usuario usuario;
    List<Usuario> usuarios;
    List<Ponto> pontos;
    RespostaDTO respostaDTO;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setIdUsuario(1);
        usuario.setCpf("123456");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setEmail("teste@teste");
        usuario.setNomeCompleto("João das Neves");

        usuarios = new ArrayList<>();
        usuarios.add(usuario);


        ponto = new Ponto();
        ponto.setIdPonto(10);
        ponto.setTipoBatida("Entrada");

        pontos = new ArrayList<>();
        pontos.add(ponto);

        respostaDTO = new RespostaDTO();
        respostaDTO.setPonto(pontos);
        respostaDTO.setHorasTrabalhadas(2);

        ponto2 = new Ponto();
        ponto2.setUsuario(usuario);
        ponto2.setDataHoraBatida(LocalDateTime.now());
        ponto2.setIdPonto(10);
        ponto2.setTipoBatida("Entrada");
    }

    @Test
    public void testeRegistrarPontoOK() throws Exception {
        Mockito.when(pontoService.efetuarBatidaPonto(usuario.getIdUsuario(), ponto)).thenReturn(ponto);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(ponto);
        mockMvc.perform(MockMvcRequestBuilders.post("/ponto/{idUsuario}/baterponto", usuario.getIdUsuario())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testeRegistrarPontoErrado() throws Exception {
        Mockito.when(pontoService.efetuarBatidaPonto(usuario.getIdUsuario(), ponto2)).thenReturn(ponto2);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(ponto2);
        mockMvc.perform(MockMvcRequestBuilders.post("/ponto/{idUsuario}/baterponto", usuario.getIdUsuario())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    public void testeSelecionarRegistrosPorUsuario() throws Exception{
        Mockito.when(pontoService.buscarPorId(1)).thenReturn(pontos);
        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/{idUsuario}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testeSelecionarRegistrosPorUsuarioQueNaoEncontrou() throws Exception{
        pontos = null;
        Mockito.when(pontoService.buscarPorId(Mockito.anyInt())).thenReturn(pontos);
        mockMvc.perform(MockMvcRequestBuilders.get("/ponto/{idUsuario}",Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
