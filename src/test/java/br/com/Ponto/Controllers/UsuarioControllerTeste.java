package br.com.Ponto.Controllers;

import br.com.Ponto.controllers.UsuarioController;
import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.UsuarioRepository;
import br.com.Ponto.services.PontoService;
import br.com.Ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = UsuarioController.class)
public class UsuarioControllerTeste {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private PontoService pontoService;

    @Autowired
    private MockMvc mockMvc;


    Usuario usuario;
    Usuario usuario2;
    List<Usuario> usuarios;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setIdUsuario(1);
        usuario.setCpf("123456");
        usuario.setEmail("teste@teste");
        usuario.setNomeCompleto("João das Neves");

        usuario2 = new Usuario();
        usuario2.setIdUsuario(1);
        usuario2.setCpf("123456");
        usuario2.setDataCadastro(LocalDate.now());
        usuario2.setEmail("teste@teste");
        usuario2.setNomeCompleto("João das Neves");

        usuarios = new ArrayList<>();
        usuarios.add(usuario);
    }

    @Test
    public void testeBuscarTodos() throws Exception {
        Iterable<Usuario> usuarios = new ArrayList<>();
        Mockito.when(usuarioService.buscarTodos()).thenReturn(usuarios);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuario")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void testeBuscarPorId() throws Exception {
        Mockito.when(usuarioService.buscarPorIdUsuario(Mockito.anyInt())).thenReturn(usuario);
        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/{idUsuario}", Mockito.anyInt())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void testeInserirUsuario() throws Exception {
        Mockito.when(usuarioService.salvarUsuario(usuario)).thenReturn(usuario);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @Test
    public void testeInserirUsuarioComErro() throws Exception {
        Mockito.when(usuarioService.salvarUsuario(usuario2)).thenReturn(usuario2);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario2);
        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


    @Test
    public void testeAtualizarUsuario() throws Exception {
        Mockito.when(usuarioService.atualizarUsuario(usuario.getIdUsuario(), usuario)).thenReturn(usuario);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);
        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/{idUsuario}", usuario.getIdUsuario())
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }


    @Test
    public void testeAtualizarUsuariocomErro() throws Exception {
        Mockito.when(usuarioService.atualizarUsuario(2, usuario2)).thenReturn(usuario2);
        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario2);
        mockMvc.perform(MockMvcRequestBuilders.put("/usuario/{idUsuario}", 2)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
