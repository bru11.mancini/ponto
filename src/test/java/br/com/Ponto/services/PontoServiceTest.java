package br.com.Ponto.services;

import br.com.Ponto.models.Ponto;
import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.PontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@SpringBootTest
public class PontoServiceTest {

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    Ponto ponto;
    Usuario usuario;
    List<Ponto> pontos;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setIdUsuario(1);
        usuario.setCpf("123456");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setEmail("teste@teste");
        usuario.setNomeCompleto("João das Neves");

        ponto = new Ponto();
        ponto.setUsuario(usuario);
        ponto.setDataHoraBatida(LocalDateTime.now());
        ponto.setIdPonto(1);
        ponto.setTipoBatida("Entrada");

        pontos = new ArrayList<>();
        pontos.add(ponto);
    }

    @Test
    public void testarEfetuarBatidaPonto(){
        Mockito.when(pontoRepository.save(ponto)).thenReturn(ponto);
        Ponto objPonto = pontoService.efetuarBatidaPonto(1, ponto);
        Assertions.assertSame(ponto, objPonto);
    }

    @Test
    public void testarEfetuarBatidaPontoErrado(){
        Ponto ponto = new Ponto();
        Mockito.when(pontoRepository.save(ponto)).thenReturn(ponto);
        Ponto objPonto = pontoService.efetuarBatidaPonto(20, ponto);
        Assertions.assertNotEquals(pontos, objPonto);
    }

    @Test
    public void testarBuscarPorId() {
        Mockito.when(pontoRepository.findAllByUsuarioIdUsuario(1)).thenReturn(pontos);
        Iterable<Ponto> pontoIterable = pontoService.buscarPorId(1);
        Assertions.assertSame(pontos, pontoIterable);
    }

    @Test
    public void testarBuscarPorIdComErro() {
        Mockito.when(pontoRepository.findAllByUsuarioIdUsuario(11)).thenReturn(pontos);
        Iterable<Ponto> pontoIterable = pontoService.buscarPorId(10);
        Assertions.assertNotEquals(pontos, pontoIterable);
    }
}
