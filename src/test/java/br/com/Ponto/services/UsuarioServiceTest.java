package br.com.Ponto.services;

import br.com.Ponto.models.Ponto;
import br.com.Ponto.models.Usuario;
import br.com.Ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private PontoService pontoService;

    @Autowired
    private UsuarioService usuarioService;

    Ponto ponto;
    Usuario usuario;
    Usuario usuario2;
    List<Usuario> usuarios;
    List<Usuario> usuarios2;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setIdUsuario(1);
        usuario.setCpf("123456");
        usuario.setDataCadastro(LocalDate.now());
        usuario.setEmail("teste@teste");
        usuario.setNomeCompleto("João das Neves");

        usuario2 = new Usuario();
        usuario2.setCpf("123456");
        usuario2.setEmail("teste@teste");
        usuario2.setNomeCompleto("João das Neves");

        usuarios = new ArrayList<>();
        usuarios.add(usuario);

        usuarios2 = new ArrayList<>();
        usuarios2.add(usuario2);


        ponto = new Ponto();
        ponto.setUsuario(usuario);
        ponto.setDataHoraBatida(LocalDateTime.now());
        ponto.setIdPonto(1);
        ponto.setTipoBatida("Entrada");
    }

    @Test
    public void testarSalvarUsuario(){
        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);
        Usuario objUsuario = usuarioService.salvarUsuario(usuario);
        Assertions.assertSame(usuario, objUsuario);
    }

    @Test
    public void testarBuscarTodos(){
        Mockito.when(usuarioRepository.findAll()).thenReturn((Iterable<Usuario>) usuarios);
        Iterable<Usuario> objUsuario = usuarioService.buscarTodos();
        Assertions.assertSame(usuarios, objUsuario);
    }

    @Test
    public void testarBuscarPorId(){
        Usuario usuario = new Usuario();
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario objUsuario = usuarioService.buscarPorIdUsuario(usuario.getIdUsuario());
        Assertions.assertSame(usuario, objUsuario);
    }


    @Test
    public void testarAtualizarUsuario2(){
        Mockito.when(usuarioRepository.existsById(1)).thenReturn(true);
        usuario.setIdUsuario(1);
        Usuario objUsuario = usuarioService.atualizarUsuario(1, usuario);
        Assertions.assertNotEquals(usuario, objUsuario);
    }

    @Test
    public void testarSalvarUsuarioErro(){
        Mockito.when(usuarioRepository.save(usuario2)).thenReturn(usuario2);
        Usuario objUsuario = usuarioService.salvarUsuario(usuario2);
        Assertions.assertNotEquals(usuario, objUsuario);
    }

    @Test
    public void testarBuscarTodosErro(){
        Mockito.when(usuarioRepository.findAll()).thenReturn((Iterable<Usuario>) usuarios2);
        Iterable<Usuario> objUsuario = usuarioService.buscarTodos();
        Assertions.assertNotEquals(usuarios, objUsuario);
    }

    @Test
    public void testarBuscarPorIdErro(){
        Usuario usuario = new Usuario();
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario objUsuario = usuarioService.buscarPorIdUsuario(usuario.getIdUsuario());
        Assertions.assertNotEquals(usuario2, objUsuario);
    }
}


